# digiKam

I have switched from AppImage to PPA.
There seems to also be a Flatpak: `org.kde.digikam`.
https://flathub.org/apps/details/org.kde.digikam


## Dependencies

This might have been a problem only on Debian, but we had to install
the missing dependency `libopenal1` to fix a startup error.



## Suggestions

Should we configure a remote database?

+ https://scribblesandsnaps.wordpress.com/2018/10/19/use-digikam-with-a-nas-and-mariadb/
+ https://discuss.pixls.us/t/use-digikam-with-a-nas-and-mariadb/9467/12
+ https://reddit.com/r/synology/comments/cxuxsb/my_digikam_setup_what_do_you_think/
+ https://github.com/roleohibachi/digikam-synology-config
+ https://www.supertechcrew.com/improving-speed-digikam-using-mysql/
+ https://scribblesandsnaps.wordpress.com/2010/10/15/using-digikam-with-mysql/

All these guides and reports seem to assume strictly inside LAN use of Digikam,
because no attempt is made to secure the MySQL port. Also, the performance demands
of Digikam on the network connection to the SQL database appear to be taxing enough
that several mentions of avoiding Wifi are made.

For my part, I like the idea of a central SQL database for Digikam, but only
if I can have the same functionality as now. Today I can work in Digikam on any
stationary or even laptop, whether on the LAN or outside it, thanks to both the
photos themselves and the SQLite database being mounted and synced via Nextcloud.

It seems a central SQL database would only work securely inside the LAN.
Digikam SQL settings do not appear to support `~/.ssh/config` or similar, which
would allow us a way to access SQL on a machine without exposing that port to
the world.

More complex solutions are possible, such as accessing the SQL host via VPN,
but that's not something I have considered yet.

It seems our Digikam is stuck with SQLite for the foreseeable future.
